FROM tomcat:latest

# Instala o Git no contêiner
RUN apt-get update && apt-get install -y git openjdk-19-jdk

# Define o diretório de trabalho dentro do contêiner
WORKDIR /app

# Clona o repositório do GitHub
RUN git clone https://github.com/francescokrum/Teatro.git

# Define o diretório de trabalho para o código clonado
WORKDIR /app/Teatro

# Compila o projeto Java
RUN javac -d /app/out src/webserver/*.java src/classes/*.java

# Define o comando padrão a ser executado quando o contêiner for iniciado
ENTRYPOINT ["java", "-cp", "/app/out", "webserver.Server"]
